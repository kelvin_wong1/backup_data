
def return_archiving_stats_dict(time_taken, file_path, size):
    """
    Returns a dict of archiving stats
    """
    stats_dict = {}
    stats_dict['time_taken'] = time_taken
    stats_dict['file_path'] = file_path
    stats_dict['size'] = size

    return stats_dict