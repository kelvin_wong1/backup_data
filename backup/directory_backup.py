import os, time, logging, tarfile, backup_util

class DirectoryBackup(object):
    def __init__(self, target=None, destination=None):
        self.target = target
        self.destination = destination
        self.backup_dir_name = "data_backup_{}"
        
        logging.basicConfig(level=logging.INFO)

    def set_target(self, target):
        self.target = target
    
    def set_destination(self, destination):
        self.destination = destination

    def backup(self):
        self._validate_target_directory()

        start_time = time.time()
        backup_dir = os.path.join(self.destination, self.backup_dir_name.format(start_time))
        backup_file_path = os.path.join(backup_dir, os.path.basename(self.target) + "_{}.tar.gz".format(start_time))

        self._create_directory_in_dest(backup_dir)
        file_size = self._create_archive_of_target_directory(backup_file_path)
        end_time = time.time()

        return backup_util.return_archiving_stats_dict((start_time - end_time), backup_file_path, file_size)

    def _validate_target_directory(self):
        """ Validates that the target path is set
        Fails if  target directory is not set or
        if target directory is a file
        """
        assert self.target != None, "Target directory was not set"
        assert os.path.isdir(self.target), "Only archiving directories is currently supported"

    def _create_directory_in_dest(self, backup_dir):
        """Creates a directory in destination path
        Named `backup_dir_name`_<timestamp_in_utc>
        Throws ValueError if destination directory exists and is not empty.
        """
        logging.info("Creating directory for archive file. path={}".format(backup_dir))
        os.mkdir(backup_dir)
    
    def _create_archive_of_target_directory(self, backup_file_path):
        """Creates tar.gz of the target directory in the destination path
        """
        with tarfile.open(backup_file_path, "w:gz") as tar:
            tar.add(self.target, arcname=os.path.basename(self.target))

        return os.path.getsize(backup_file_path)