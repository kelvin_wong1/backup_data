import time, subprocess, os, logging, tarfile, backup_util

class MysqlDump(object):
    def __init__(self, db_host, db_user, db_pass, dest):
        self.db_host = db_host
        self.db_user = db_user
        self.db_pass = db_pass
        self.dest = dest
    
    def run_mysql_dump(self):
        backup_file_path = self._create_backup_file_path()
        start_time = time.time()
        process = subprocess.Popen("mysqldump -h {} -u {} -p'{}' --all-databases > {}".format(self.db_host, self.db_user, self.db_pass, backup_file_path), shell=True)
        process.communicate()

        if (process.returncode != 0):
            logging.error("MySQL dump failed")
        
        with tarfile.open(backup_file_path + ".tar.gz", "w:gz") as tar:
            tar.add(backup_file_path, arcname=os.path.basename(backup_file_path))
        end_time = time.time()

        file_size = os.path.getsize(backup_file_path)

        return backup_util.return_archiving_stats_dict((start_time - end_time), backup_file_path, file_size)
    
    def _create_backup_file_path(self):
        ts = str(int(time.time()))

        backup_dir = os.path.join(self.dest, "sql_{}".format(ts))
        os.mkdir(backup_dir)

        sql_backup_file = os.path.join(backup_dir, "database_backup_{}.sql".format(ts))

        return sql_backup_file
